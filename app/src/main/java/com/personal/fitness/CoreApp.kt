package com.personal.fitness

import android.app.Application
import com.personal.fitness.utils.Prefs
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoreApp: Application() {
    override fun onCreate() {
        super.onCreate()
        prefs = Prefs(applicationContext)
    }

    companion object {
        lateinit var prefs: Prefs
    }
}