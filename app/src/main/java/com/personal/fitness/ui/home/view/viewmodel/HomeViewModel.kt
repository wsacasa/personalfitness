package com.personal.fitness.ui.home.view.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.personal.fitness.data.FirestoreManager
import com.personal.fitness.data.home.WorkoutData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor() : ViewModel() {
    companion object {
        private const val TAG : String = "HomeViewModel"
    }

    private val _title = MutableLiveData<String>()
    val title: LiveData<String> = _title

    private val _reps = MutableLiveData<Int>()
    val reps: LiveData<Int> = _reps

    private val _series = MutableLiveData<Int>()
    val series: LiveData<Int> = _series

    private val _body = MutableLiveData<String>()
    val body: LiveData<String> = _body

    private val _description = MutableLiveData<String>()
    val description: LiveData<String> = _description

    /** Complete Data Fragment Var */
    /** ***************************************************************************************** */
    private val _name = MutableLiveData<String>()
    val name: LiveData<String> = _name

    private val _age = MutableLiveData<Int>()
    val age: LiveData<Int> = _age

    private val _height = MutableLiveData<Int>()
    val height: LiveData<Int> = _height

    private val _weight = MutableLiveData<Float>()
    val weight: LiveData<Float> = _weight

    private val _gender = MutableLiveData<String>()
    val gender: LiveData<String> = _gender

    private val _code = MutableLiveData<String>()
    val code: LiveData<String> = _code

    private val _phone = MutableLiveData<String>()
    val phone: LiveData<String> = _phone
    /** ***************************************************************************************** */

    val phoneState = MutableLiveData<Boolean>()

    fun onTitleChanged(title: String) { _title.value = title }

    fun onRepsChanged(reps: Int) { _reps.value = reps }

    fun onSeriesChanged(series: Int) { _series.value = series }

    fun onBodyChanged(body: String) { _body.value = body }

    fun onDescriptionChanged(description: String) { _description.value = description }

    fun saveWorkout(data: WorkoutData?) {
        val firestore = FirestoreManager(null, this)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                if (data != null)
                    firestore.addWorkout(data)
                else
                    Log.d(TAG, "saveWorkout: Leer comentarios")
            }
        }
    }

    fun workoutSaved(data: String) {
        /** Hay que mostrar ventana de enviando datos */
    }

    /** Complete Data Fragment Functions */
    /** ***************************************************************************************** */
    fun onAgeSelected(age: Int) {
        if (age != _age.value) _age.value = age
    }

    fun onNamedChanged(name: String) {
        _name.value = name
    }

    fun onHeightSelected(height: Int) {
        if (height != _height.value) _height.value = height
    }

    fun onWeightSelected(weight: Float) {
        _weight.value = weight
    }

    fun onGenderChanged(gender: String) {
        _gender.value = gender
    }
    /** ***************************************************************************************** */

    fun onCodePhone(code: String) {
        //phoneState.postValue(true)
        _code.value = code
    }

    fun onPhoneNumber(phone: String) {
        _phone.value = phone
    }

}