package com.personal.fitness.ui.home.view.activities

import android.Manifest
import android.Manifest.permission.READ_PHONE_NUMBERS
import android.Manifest.permission.READ_PHONE_STATE
import android.Manifest.permission.READ_SMS
import android.accounts.Account
import android.accounts.AccountManager
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.core.app.ActivityCompat
import com.personal.fitness.ui.home.view.ui.HomeScreen
import com.personal.fitness.ui.home.view.viewmodel.HomeViewModel
import com.personal.fitness.ui.theme.PersonalFitnessTheme
import dagger.hilt.android.AndroidEntryPoint
import java.lang.IllegalStateException


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    /** ViewModel Variables */
    private val homeViewModel: HomeViewModel by viewModels()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PersonalFitnessTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.onBackground
                ) {
                    HomeScreen(supportFragmentManager)
                }
            }
        }

        homeViewModel.phoneState.observe(this){
            if (it) {
                Log.d("MainActivity", "Call Function getNumber()")
                //getNumber()
                /*val am = AccountManager.get(this)
                val accounts: Array<Account> = am.accounts
                val googleAccounts: ArrayList<*> = ArrayList<Any?>()

                for (ac in accounts) {
                    val acname = ac.name
                    val actype = ac.type
                    Log.d("MainActivity", "Accounts: $acname, $actype")
                    println("Accounts : $acname, $actype")
                    if (actype == "com.whatsapp") {
                        val phoneNumber = ac.name
                    }
                }*/
            }
        }
    }

    /**
    @RequiresApi(Build.VERSION_CODES.O)
    private fun getNumber() {
        if (
            ActivityCompat.checkSelfPermission(this, READ_SMS) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, READ_PHONE_NUMBERS) == PackageManager.PERMISSION_GRANTED) {
            val telephonyManager = this.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
            val phoneNumber = telephonyManager.toString()
            Log.d("MainActivity", "getNumber: $phoneNumber")
            Log.d("MainActivity", "getNumber: ${telephonyManager}")
            return
        } else {
            requestPermission()
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun requestPermission() {
        requestPermissions(arrayOf(READ_SMS, READ_PHONE_NUMBERS, READ_PHONE_STATE), 100)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            100 -> {
                val telephonyManager = this.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
                if (ActivityCompat.checkSelfPermission(this, READ_SMS) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return
                }
                val phoneNumber = telephonyManager.line1Number
                Log.d("MainActivity", "onRequestPermissionsResult: $phoneNumber")
            }
            else -> {
                throw IllegalStateException("Unexpected value: $requestCode")
            }
        }
    }
    */
}