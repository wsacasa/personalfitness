package com.personal.fitness.ui.login.viewmodel

import android.util.Log
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.personal.fitness.CoreApp
import com.personal.fitness.CoreApp.Companion.prefs
import com.personal.fitness.data.FirestoreManager
import com.personal.fitness.data.login.CustomDialogData
import com.personal.fitness.data.login.LoginData
import com.personal.fitness.data.login.LoginResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor() : ViewModel() {

    companion object {
        private const val TAG: String = "LoginViewModel"
    }

    private val _email = MutableLiveData<String>()
    val email: LiveData<String> = _email

    private val _pass = MutableLiveData<String>()
    val pass: LiveData<String> = _pass

    private val _loginEnabled = MutableLiveData<Boolean>()
    val loginEnabled: LiveData<Boolean> = _loginEnabled

    private val _nextData = MutableLiveData<Int>()
    val nextData: LiveData<Int> = _nextData

    private val _code = MutableLiveData<String>()
    val code: LiveData<String> = _code

    private val _googleEnabled = MutableLiveData<Boolean>()
    val googleEnabled: LiveData<Boolean> = _googleEnabled

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _showDialog = MutableLiveData<CustomDialogData>()
    val showDialog: LiveData<CustomDialogData> = _showDialog

    val googleSingI = MutableLiveData<Boolean>()

    val firebaseLogin = MutableLiveData<LoginData>()

    val userSuccessfullySave = MutableLiveData<LoginResponse>()

    fun onLoginChanged(email: String, pass: String) {
        _email.value = email
        _pass.value = pass
        _loginEnabled.value = isValidEmail(email) && isValidPass(pass)
    }

    private fun isValidPass(pass: String): Boolean = pass.length > 7

    private fun isValidEmail(email: String): Boolean = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    suspend fun onLoginSelected(loginData: LoginData) {
        _isLoading.value = true
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                firebaseLogin.postValue(loginData)
            }
        }
    }

    fun submitData(data: LoginData) {
        val firestore = FirestoreManager(this, null)
        /** This code call list from firebase database into Flow */
        //val users by firestore.getUserFlow(data).collectAsState(initial = emptyList())
        /** Add user to Firebase */
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                firestore.addUser(data)
            }
        }
    }

    fun initFirebaseData(identity: String) {
        onLoadingConfirm()
        val firestore = FirestoreManager(this, null)
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                firestore.getUserFlow(identity).collect { user ->
                    user.map {
                        val data = it
                        val user = LoginResponse(state = 1, data = data)
                        userSuccessfullySave.postValue(user)
                        Log.d(TAG, "initFirebaseData: ${data.doc}")
                    }
                }
            }
        }
    }

    fun googleSign() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                googleSingI.postValue(true)
            }
        }
    }

    fun showDialog(title: String, content: String, state: Boolean, data: LoginData?) {
        if (state) {
            _showDialog.value = CustomDialogData(title = title, content = content, state = state, user = data)
        }
    }

    fun userSaved(user: LoginData) {
        val response = LoginResponse(state = 0, data = user)
        userSuccessfullySave.postValue(response)
    }

    fun nextButton(state: Int, value: String) {
        _nextData.value = state
        when (state) {
            1 -> {
                prefs.saveUserName(value)
            }
            2 -> {

            }
        }
    }

    fun onCodePhone(code: String) {
        _code.value = code
    }

    fun onLoadingConfirm() {
        _isLoading.value = true
    }

    fun onLoadingDismiss() {
        /** Optional with suspend fun */
        //delay(4000)
        _isLoading.value = false
    }


}