package com.personal.fitness.ui.login.view.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import com.personal.fitness.CoreApp.Companion.prefs
import com.personal.fitness.R
import com.personal.fitness.data.login.CustomDialogData
import com.personal.fitness.data.login.LoginData
import com.personal.fitness.ui.login.viewmodel.LoginViewModel
import kotlinx.coroutines.launch

@Composable
fun LoginScreen(viewModel: LoginViewModel) {
    Box(modifier = Modifier
        .fillMaxSize()
        .padding(16.dp)) {
        Login(Modifier.align(Alignment.Center), viewModel)
    }
}

@Composable
fun Login(modifier: Modifier, viewModel: LoginViewModel) {
    var show by rememberSaveable { mutableStateOf(false) }
    val email : String by viewModel.email.observeAsState(initial = "")
    val pass : String by viewModel.pass.observeAsState(initial = "")
    val loginEnabled : Boolean by viewModel.loginEnabled.observeAsState(initial = false)
    //val nextData : Boolean by viewModel.nextData.observeAsState(initial = false)
    val googleEnabled : Boolean by viewModel.googleEnabled.observeAsState(initial = true)
    val showDialog : CustomDialogData? by viewModel.showDialog.observeAsState(initial = null)
    val isLoading : Boolean by viewModel.isLoading.observeAsState(initial = false)
    val coroutineScope = rememberCoroutineScope()
    var loginData: LoginData?
    showDialog.let {
        if (it != null) loginData = it.user else null
    }
    show = showDialog?.state ?: false

    if (isLoading) {
        Box(modifier = Modifier.fillMaxSize()) {
            CircularProgressIndicator(Modifier.align(Alignment.Center))
        }
    } else {
        Column(modifier = modifier) {
            HeaderImage(Modifier.align(Alignment.CenterHorizontally))
            Spacer(modifier = Modifier.padding(18.dp))
            VisualField("Email", email) { viewModel.onLoginChanged(it, pass) }
            Spacer(modifier = Modifier.padding(12.dp))
            VisualField("Password", pass) { viewModel.onLoginChanged(email, it) }
            Spacer(modifier = Modifier.padding(10.dp))
            ForgotPassword(Modifier.align(Alignment.End))
            Spacer(modifier = Modifier.padding(10.dp))
            LoginButton(loginEnabled) {
                loginData = LoginData(user = email, pass = pass, type = "firebase", "", "", "")
                coroutineScope.launch {
                    viewModel.onLoginSelected(loginData!!)
                }
            }
            Spacer(modifier = Modifier.padding(10.dp))
            Text(
                text = stringResource(id = R.string.str_social_network),
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally),
                color = colorResource(id = R.color.white),
                textAlign = TextAlign.Center
            )
            Spacer(modifier = Modifier.padding(10.dp))
            ConstraintLayoutContent(googleEnabled, viewModel)
        }
        /*if (nextData) {
            NextDialog(nextData, viewModel)
        }*/
    }
}

@Composable
fun ConstraintLayoutContent(googleEnabled: Boolean, viewModel: LoginViewModel) {
    val coroutineScope = rememberCoroutineScope()
    ConstraintLayout(modifier = Modifier.fillMaxWidth()) {
        val (google, facebook) = createRefs()
        Button(
            onClick = {
                coroutineScope.launch {
                    viewModel.googleSign()
                }
                      },
            modifier = Modifier
                .width(150.dp)
                .height(50.dp)
                .constrainAs(google) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(facebook.start)
                },
            shape = RoundedCornerShape(3.dp),
            colors = ButtonDefaults.buttonColors(
                containerColor = colorResource(id = R.color.white),
                disabledContainerColor = colorResource(id = R.color.gray),
                contentColor = colorResource(id = R.color.black),
                disabledContentColor = Color.White
            ),
            enabled = googleEnabled
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_google),
                contentDescription = "Google Button",
                contentScale = ContentScale.FillHeight
            )
        }

        Button(
            onClick = {
                /*coroutineScope.launch {
                    viewModel.googleSign()
                }*/
            },
            modifier = Modifier
                //.fillMaxWidth()
                .width(150.dp)
                .height(50.dp)
                .constrainAs(facebook) {
                    top.linkTo(parent.top)
                    start.linkTo(google.end)
                    end.linkTo(parent.end)
                },
            shape = RoundedCornerShape(3.dp),
            colors = ButtonDefaults.buttonColors(
                containerColor = colorResource(id = R.color.colorFacebook),
                disabledContainerColor = colorResource(id = R.color.gray),
                contentColor = colorResource(id = R.color.black),
                disabledContentColor = Color.White
            ),
            enabled = googleEnabled
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_facebook),
                contentDescription = "Facebook Button",
                contentScale = ContentScale.FillHeight
            )
        }

        createHorizontalChain(google, facebook, chainStyle = ChainStyle.SpreadInside)

        /*GoogleButton(googleEnabled) {
            coroutineScope.launch {
                viewModel.googleSign()
            }
        }*/
    }
}

/*@Composable
fun GoogleButton(googleEnabled: Boolean, googleSign: () -> Unit) {
    Button(
        onClick = { googleSign() },
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp),
        shape = RoundedCornerShape(3.dp),
        colors = ButtonDefaults.buttonColors(
            containerColor = colorResource(id = R.color.white),
            disabledContainerColor = colorResource(id = R.color.gray),
            contentColor = colorResource(id = R.color.black),
            disabledContentColor = Color.White
        ),
        enabled = googleEnabled
        ) {

        Image(
            painter = painterResource(id = R.drawable.ic_google),
            contentDescription = "Google Button"
        )
    }
}
*/

@Composable
fun LoginButton(loginEnabled: Boolean, onLoginSelected: () -> Unit) {
    Button(
        onClick = { onLoginSelected() },
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp),
        shape = RoundedCornerShape(3.dp),
        colors = ButtonDefaults.buttonColors(
            containerColor = colorResource(id = R.color.colorBlueDark),
            disabledContainerColor = colorResource(id = R.color.gray),
            contentColor = Color.White,
            disabledContentColor = Color.White
        ),
        enabled = loginEnabled
    ) {
        Text(text = "Login")
    }
}

@Composable
fun ForgotPassword(modifier: Modifier) {
    Text(
        text = "Forgot your password?",
        modifier = modifier.clickable { },
        fontSize = 16.sp,
        fontWeight = FontWeight.ExtraBold,
        fontStyle = FontStyle(1),
        color = colorResource(id = R.color.white) //Color(0xFFFB9600)
    )
}

@Composable
fun VisualField(placeholder: String, data: String, onTextFieldChanged: (String) -> Unit) {
    val keyboardOptions = KeyboardOptions
    var keyboardType = keyboardOptions.Default.keyboardType
    when (placeholder) {
        "Email" -> {
            keyboardType = KeyboardType.Email
        }

        "Password" -> {
            keyboardType = KeyboardType.Password
        }
    }
    TextField(
        value = data,
        onValueChange = {onTextFieldChanged(it)},
        modifier = Modifier.fillMaxWidth(),
        placeholder = { Text(text = placeholder) },
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType),
        singleLine = true,
        maxLines = 1,
        colors = TextFieldDefaults.colors(
            focusedContainerColor = Color(0xFFDEDDDD), //0xFF636262-0xFFDEDDDD
            focusedIndicatorColor = Color.Transparent, unfocusedIndicatorColor = Color.Transparent
        )
    )
}

@Composable
fun HeaderImage(modifier: Modifier) {
    Image(
        painter = painterResource(id = R.drawable.fitness_logo),
        contentDescription = "Header",
        modifier = modifier
            .height(220.dp)
            .width(220.dp)
    )
}

@Composable
fun CustomDialog(show: Boolean, title: String, content: String, onDismiss: () -> Unit, onConfirm: () -> Unit) {
    if (show) {
        AlertDialog(onDismissRequest = {
            onDismiss()
                                       },
            confirmButton = {
                TextButton(onClick = { onConfirm() }) {
                    Text(text = "ConfirmButton")
                }
            },
            dismissButton = {
                TextButton(onClick = { onDismiss() }) {
                    Text(text = "DismissButton")
                }
            },
            title = { Text(text = title) },
            text = { Text(text = content) }) //stringResource(R.string.str_msg_error)
    }
}

/*@Composable
fun NextDialog(nextData: Boolean, viewModel: LoginViewModel) {
    if (nextData) {
        val name = prefs.getSharedName() ?: stringResource(id = R.string.str_next_data_name)
        NextDataScreen(name, viewModel)
        //AlertDialog(onDismissRequest = { /*TODO*/ }, confirmButton = { /*TODO*/ })
    }
}*/
