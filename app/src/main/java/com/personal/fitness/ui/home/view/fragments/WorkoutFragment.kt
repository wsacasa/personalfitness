package com.personal.fitness.ui.home.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.personal.fitness.ui.home.view.ui.WorkoutScreen
import com.personal.fitness.ui.home.view.viewmodel.HomeViewModel
import com.personal.fitness.ui.theme.PersonalFitnessTheme

class WorkoutFragment: Fragment() {
    private val homeViewModel: HomeViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                PersonalFitnessTheme {
                    WorkoutScreen(homeViewModel)
                }
            }
        }
    }
}