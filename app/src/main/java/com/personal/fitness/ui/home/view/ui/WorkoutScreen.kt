package com.personal.fitness.ui.home.view.ui

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExtendedFloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.personal.fitness.R
import com.personal.fitness.data.home.WorkoutData
import com.personal.fitness.ui.home.view.viewmodel.HomeViewModel

@Composable
fun WorkoutScreen(homeModel: HomeViewModel) {
    Scaffold /*(floatingActionButton = { Fab() },floatingActionButtonPosition = FabPosition.End)*/ {
        Surface(modifier = Modifier
            .fillMaxSize()
            .padding(it)) {
            Content(viewModel = homeModel)
        }
    }
}

@Composable
fun Content(viewModel: HomeViewModel) {
    val title: String by viewModel.title.observeAsState(initial = "")
    val description: String by viewModel.description.observeAsState(initial = "")
    LazyColumn(modifier = Modifier
        .fillMaxSize()
        .background(colorResource(id = R.color.gray_lighter))) {
        item {
            MainCard(title, description, viewModel)
        }
    }
}

@Composable
fun MainCard(title: String, description: String, viewModel: HomeViewModel) {
    Column (modifier = Modifier.padding(25.dp)) {
        CardWorkout(text = stringResource(id = R.string.str_workout_title), placeholder = stringResource(id = R.string.str_workout_title_placeholder), data = title, section = "title", viewModel = viewModel)
        ConstrainContent(viewModel)
        Spacer(modifier = Modifier.padding(8.dp))
        ConstrainContent2(viewModel)
        Spacer(modifier = Modifier.padding(8.dp))
        CardWorkout(text = stringResource(id = R.string.str_workout_description), placeholder = stringResource(id = R.string.str_workout_description_placeholder), data = description, section = "description", viewModel = viewModel)
        Spacer(modifier = Modifier.padding(8.dp))
        ActionButton(viewModel)
    }
}

@Composable
fun ActionButton(viewModel: HomeViewModel) {
    Row (modifier = Modifier.fillMaxWidth()) {
        Column (modifier = Modifier.weight(1f)) {
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp)
                    .padding(horizontal = 10.dp),
                colors = ButtonDefaults.buttonColors(
                    containerColor = colorResource(id = R.color.black_background),
                    contentColor = colorResource(id = R.color.white),
                    disabledContainerColor = Color.Transparent,
                    disabledContentColor = Color.Transparent
                ),
                onClick = {  }) {
                Text(text = "Picture")
            }
        }
        Column (modifier = Modifier.weight(1f)) {
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(50.dp)
                    .padding(horizontal = 10.dp),
                colors = ButtonDefaults.buttonColors(
                    containerColor = colorResource(id = R.color.black_background),
                    contentColor = colorResource(id = R.color.white),
                    disabledContainerColor = Color.Transparent,
                    disabledContentColor = Color.Transparent
                ),
                onClick = {
                    /** Obtener los valores de las cajas de texto en variables, validando desde el homeViewModel si ya fueron enviados
                     * y las variables no tienen valores genericos y una vez guardados los datos, resetear los valores del model genericos
                     * para el siguiente save record de workout */
                    val data = null //WorkoutData()
                    viewModel.saveWorkout(data)
                }) {
                Text(text = "Save")
            }
        }
    }
}

@Composable
fun ConstrainContent(viewModel: HomeViewModel) {
    Card(modifier = Modifier
        .fillMaxWidth()
        .height(85.dp),
        colors = CardDefaults.cardColors(
            containerColor = Color.Transparent,//colorResource(id = R.color.black_background),
            contentColor = colorResource(id = R.color.gray),
            disabledContentColor = colorResource(id = R.color.gray),
            disabledContainerColor = colorResource(id = R.color.colorBlueDark)
        )
    ) {
        Row(modifier = Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.weight(2f)) {
                Text(text = stringResource(id = R.string.str_workout_body), fontWeight = FontWeight.Bold, modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 10.dp, bottom = 10.dp))
                Card(shape = RoundedCornerShape(5.dp),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 10.dp),
                    colors = CardDefaults.cardColors(
                        containerColor = colorResource(id = R.color.white),
                        contentColor = colorResource(id = R.color.black_background),
                        disabledContentColor = colorResource(id = R.color.gray),
                        disabledContainerColor = colorResource(id = R.color.colorBlueDark))
                ) {
                    DropMenu(viewModel, 2)
                }
            }
            Column(modifier = Modifier.weight(1f)) {
                Text(text = stringResource(id = R.string.str_workout_series), fontWeight = FontWeight.Bold, modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 10.dp, bottom = 10.dp))
                Card(shape = RoundedCornerShape(5.dp),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 10.dp),
                    colors = CardDefaults.cardColors(
                        containerColor = colorResource(id = R.color.white),
                        contentColor = colorResource(id = R.color.black_background),
                        disabledContentColor = colorResource(id = R.color.gray),
                        disabledContainerColor = colorResource(id = R.color.colorBlueDark))
                ) {
                    DropMenu(viewModel, 1)
                }
            }
        }
    }
}

@Composable
fun ConstrainContent2(viewModel: HomeViewModel) {
    Card(modifier = Modifier
        .fillMaxWidth()
        .height(85.dp),
        colors = CardDefaults.cardColors(
            containerColor = Color.Transparent,//colorResource(id = R.color.black_background),
            contentColor = colorResource(id = R.color.gray),
            disabledContentColor = colorResource(id = R.color.gray),
            disabledContainerColor = colorResource(id = R.color.colorBlueDark)
        )
    ) {
        Row(modifier = Modifier.fillMaxWidth()) {
            Column(modifier = Modifier.weight(2f)) {
                Text(text = stringResource(id = R.string.str_workout_machine), fontWeight = FontWeight.Bold, modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 10.dp, bottom = 10.dp))
                Card(shape = RoundedCornerShape(5.dp),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 10.dp),
                    colors = CardDefaults.cardColors(
                        containerColor = colorResource(id = R.color.white),
                        contentColor = colorResource(id = R.color.black_background),
                        disabledContentColor = colorResource(id = R.color.gray),
                        disabledContainerColor = colorResource(id = R.color.colorBlueDark))
                ) {
                    DropMenu(viewModel, 3)
                }
            }
            Column(modifier = Modifier.weight(1f)) {
                Text(text = stringResource(id = R.string.str_workout_reps), fontWeight = FontWeight.Bold, modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 10.dp, bottom = 10.dp))
                Card(shape = RoundedCornerShape(5.dp),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 10.dp),
                    colors = CardDefaults.cardColors(
                        containerColor = colorResource(id = R.color.white),
                        contentColor = colorResource(id = R.color.black_background),
                        disabledContentColor = colorResource(id = R.color.gray),
                        disabledContainerColor = colorResource(id = R.color.colorBlueDark))
                ) {
                    DropMenu(viewModel, 0)
                }
            }
        }
    }
}

@Composable
fun DropMenu(viewModel: HomeViewModel, options: Int) {
    val nReps = arrayOf(6, 8, 10, 12, 15)
    val nSeries = arrayOf(3, 4, 5, 6, 7)
    val nBody = arrayOf("Chest", "Back", "Shoulders", "Legs", "Biceps", "Triceps")
    val nInstrument = arrayOf("Dumbbells", "Barbell", "Pulley", "Leg Machine", "Chest Machine", "Back Machine", "Other")
    var expanded by remember { mutableStateOf(false) }
    var comboReps by rememberSaveable { mutableIntStateOf(6) }
    var comboSeries by rememberSaveable { mutableIntStateOf(3) }
    var comboBody by rememberSaveable { mutableStateOf("Chest") }
    var comboInstrument by rememberSaveable { mutableStateOf("Dumbbells") }
    var comboSelect by rememberSaveable { mutableStateOf("") }
    Row(modifier = Modifier.fillMaxWidth()) {
        Column(modifier = Modifier.weight(2f)) {
            Box(modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.CenterHorizontally)
            ) {
                when (options) {
                    0 -> { comboSelect = comboReps.toString() }
                    1 -> { comboSelect = comboSeries.toString() }
                    2 -> { comboSelect = comboBody }
                    3 -> { comboSelect = comboInstrument }
                }
                Text(
                    text = comboSelect,
                    textAlign = TextAlign.Center,
                    color = colorResource(id = R.color.black),
                    fontSize = 18.sp,
                    modifier = Modifier
                        .fillMaxSize()
                        .wrapContentHeight(align = Alignment.CenterVertically)
                )
            }
        }
        Column(modifier = Modifier.weight(1f)) {
            Box(modifier = Modifier.fillMaxSize()) {
                IconButton(modifier = Modifier.fillMaxSize(), onClick = { expanded = !expanded }) {
                    Icon(imageVector = Icons.Default.ArrowDropDown, contentDescription = "Reps", tint = colorResource(id = R.color.black))
                }
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }) {
                    when (options) {
                        0 -> {
                            for (rps in nReps) {
                                DropdownMenuItem(text = { Text(text = rps.toString()) }, onClick = {
                                    viewModel.onRepsChanged(rps)
                                    expanded = false
                                    comboReps = rps
                                })
                            }
                        }
                        1 -> {
                            for (rps in nSeries) {
                                DropdownMenuItem(text = { Text(text = rps.toString()) }, onClick = {
                                    viewModel.onSeriesChanged(rps)
                                    expanded = false
                                    comboSeries = rps
                                })
                            }
                        }
                        2 -> {
                            for (rps in nBody) {
                                DropdownMenuItem(text = { Text(text = rps) }, onClick = {
                                    viewModel.onBodyChanged(rps)
                                    expanded = false
                                    comboBody = rps
                                })
                            }
                        }
                        3 -> {
                            for (rps in nInstrument) {
                                DropdownMenuItem(text = { Text(text = rps) }, onClick = {
                                    viewModel.onBodyChanged(rps)
                                    expanded = false
                                    comboInstrument = rps
                                })
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun CardWorkout(text: String, placeholder: String, data: String, section: String, viewModel: HomeViewModel) {
    Card(
        modifier = Modifier.fillMaxWidth(),
        colors = CardDefaults.cardColors(
            containerColor = Color.Transparent,//colorResource(id = R.color.black_background),
            contentColor = colorResource(id = R.color.gray),
            disabledContentColor = colorResource(id = R.color.gray),
            disabledContainerColor = colorResource(id = R.color.colorBlueDark)
        )
    ) {
        Text(text = text, fontWeight = FontWeight.Bold, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 10.dp, bottom = 10.dp))
        VisualField(placeholder = placeholder, data = data, section = section) {
            when (section) {
                "title" -> {
                    viewModel.onTitleChanged(it)
                }
                "description" -> {
                    viewModel.onDescriptionChanged(it)
                }
            }
        }
        Spacer(modifier = Modifier.padding(6.dp))
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VisualField(placeholder: String, data: String, section: String, onTextFieldChanged: (String) -> Unit) {
    val keyboardType: KeyboardType
    val singleLineValue: Boolean
    val maxLinesValue: Int
    val cSize: Dp
    when (section) {
        "description" -> {
            cSize = 120.dp
            maxLinesValue = 6
            singleLineValue = false
            keyboardType = KeyboardType.Text
        }

        else -> {
            cSize = 55.dp
            maxLinesValue = 1
            singleLineValue = true
            keyboardType = KeyboardType.Text
        }
    }
    TextField(
        value = data,
        onValueChange = { onTextFieldChanged(it) },
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 10.dp)
            .height(cSize),
        placeholder = { Text(text = placeholder) },
        keyboardOptions = KeyboardOptions(keyboardType = keyboardType),
        singleLine = singleLineValue,
        maxLines = maxLinesValue,
        colors = TextFieldDefaults.textFieldColors(containerColor = colorResource(R.color.white))/*.colors(
                focusedContainerColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent
            ) */
    )

}

@Composable
fun Fab() {
    val context = LocalContext.current
    var show by rememberSaveable { mutableStateOf(false) }
    ExtendedFloatingActionButton(
        onClick = {
            show = true
        },
        containerColor = colorResource(id = R.color.black),
        contentColor = colorResource(id = R.color.white)
        //shape = CircleShape
    ) {
        Icon(Icons.Filled.Add, "Floating action button.")
        Text(text = "Extended FAB")
    }
    Surface(modifier = Modifier.fillMaxWidth(), color = MaterialTheme.colorScheme.background) {
        Toast.makeText(context, "Subscribe", Toast.LENGTH_LONG).show()
    }
}