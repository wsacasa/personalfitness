package com.personal.fitness.ui.login.view.ui

import android.content.Context
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.CardColors
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.personal.fitness.R
import com.personal.fitness.ui.login.viewmodel.LoginViewModel
import kotlinx.coroutines.launch

@Preview
@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CompleteName() {
    //name: String, viewModel: LoginViewModel, context: Context
    val loginViewModel = LoginViewModel()
    val name = ""
    val context: Context
    val stateNext : Int by loginViewModel.nextData.observeAsState(initial = 1)
    val countryCode : String by loginViewModel.code.observeAsState(initial = "+ 0")
    val cardColors = CardDefaults.cardColors(
        containerColor = colorResource(id = R.color.black_down),
        contentColor = colorResource(id = R.color.gray),
        disabledContentColor = colorResource(id = R.color.gray),
        disabledContainerColor = colorResource(id = R.color.colorBlueDark)
    )
    val modifierBox = Modifier
        .fillMaxWidth()
        .padding(12.dp)
    val modifierRow = Modifier.fillMaxWidth()
    /*var nameChanged by rememberSaveable { mutableStateOf("") }
    var nameHolder by rememberSaveable { mutableStateOf(name) }
    var buttonEnabled by rememberSaveable { mutableStateOf(false) }
    val coroutineScope = rememberCoroutineScope() */

    Surface(modifier = Modifier.fillMaxSize()) {
        when (stateNext) {
            0 -> {
                FullName(name, modifierBox, modifierRow, cardColors, loginViewModel)
            }
            1 -> {
                PhoneNumber(countryCode, modifierBox, modifierRow, cardColors, loginViewModel, context)
            }
        }
    }
}

@Composable
fun FullName(
    name: String,
    modifier: Modifier,
    modifierRow: Modifier,
    cardColors: CardColors,
    loginViewModel: LoginViewModel
) {
    var colorSelections: Color
    val holderText = stringResource(id = R.string.str_next_data_name)
    val coroutineScope = rememberCoroutineScope()
    var nameChanged by rememberSaveable { mutableStateOf("") }
    var nameHolder by rememberSaveable { mutableStateOf(holderText) }
    var buttonEnabled by rememberSaveable { mutableStateOf(false) }
    if (nameChanged != "") {
        buttonEnabled = true
        colorSelections = colorResource(id = R.color.colorGreenLight)
    } else {
        buttonEnabled = false
        colorSelections = colorResource(id = R.color.gray)
    }
    Card(colors = cardColors, shape = RoundedCornerShape(0.dp)) {
        Box(modifier = modifier) {
            Column(modifier = Modifier.align(Alignment.Center)) {
                Row {
                    Text(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 10.dp),
                        text = stringResource(id = R.string.str_complete_name),
                        fontWeight = FontWeight.Bold,
                        color = colorResource(id = R.color.white),
                        textAlign = TextAlign.Center,
                        fontSize = 22.sp
                    )
                }
                Space()
                if (name != stringResource(id = R.string.str_next_data_name) && name != "") {
                    nameChanged = name
                    buttonEnabled = true
                    colorSelections = colorResource(id = R.color.colorGreenLight)
                    Row(modifier = modifierRow) {
                        Column {
                            Icon(
                                modifier = Modifier
                                    .height(35.dp)
                                    .padding(end = 3.dp, bottom = 3.dp),
                                imageVector = Icons.Default.Info,
                                tint = colorResource(id = R.color.colorBlueDark),
                                contentDescription = stringResource(id = R.string.str_general_info)
                            )
                        }
                        Column {
                            Text(text = stringResource(id = R.string.str_next_data_title_info), color = colorResource(id = R.color.colorBlueDark))
                        }
                    }
                    Space()
                }
                Row(modifier = modifierRow) {
                    Column(modifier = Modifier.weight(5f)) {
                        OutlinedTextField(
                            modifier = modifierRow,
                            value = nameChanged,
                            placeholder = { Text(text = nameHolder) },
                            onValueChange = {
                                if (nameHolder != "") {
                                    nameHolder = ""
                                }
                                nameChanged = it
                            },
                            colors = OutlinedTextFieldDefaults.colors(
                                focusedTextColor = colorResource(id = R.color.white),
                                unfocusedTextColor = colorResource(id = R.color.white),
                                unfocusedBorderColor = colorResource(id = R.color.gray),
                                unfocusedLabelColor = Color.White,
                                unfocusedLeadingIconColor = Color.White
                            )
                        )
                    }
                    Column(
                        modifier = Modifier
                            .weight(1f)
                            .align(Alignment.CenterVertically)
                    ) {
                        Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                            SetIconCheck(colorSelections)
                        }
                    }
                }
                Space()
                Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                    Image(
                        painter = painterResource(id = R.drawable.pf_color),
                        contentDescription = stringResource(id = R.string.str_next_data_name),
                        modifier = Modifier.fillMaxWidth()
                    )
                }
                Space()
                Row {
                    NextButton(buttonEnabled) {
                        if (nameChanged != "") {
                            coroutineScope.launch {
                                loginViewModel.nextButton(1, nameChanged)
                            }
                        } else {
                            Log.d("CompleteName", "CompleteName: $name")
                            //Toast.makeText(context, "", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }

        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun PhoneNumber(
    countryCode: String,
    modifier: Modifier,
    modifierRow: Modifier,
    cardColors: CardColors,
    loginViewModel: LoginViewModel,
    context: Context
) {
    var phoneNumber = ""
    var colorSelections: Color
    val showDialog = remember { mutableStateOf(false) }
    var buttonEnabled by rememberSaveable { mutableStateOf(false) }
    val coroutineScope = rememberCoroutineScope()
    if (phoneNumber != "") {
        buttonEnabled = true
        colorSelections = colorResource(id = R.color.colorGreenLight)
    } else {
        buttonEnabled = false
        colorSelections = colorResource(id = R.color.gray)
    }
    if (showDialog.value) {
        CountryDialog(context = context, viewModel = null, loginModel = loginViewModel, setShowDialog = { showDialog.value = it })
    }
    Card(colors = cardColors, shape = RoundedCornerShape(0.dp)){
        Box(modifier = modifier) {
            Column {
                Row {
                    Text(
                        modifier = Modifier.fillMaxWidth().padding(top = 10.dp),
                        text = stringResource(id = R.string.str_phone_number),
                        fontWeight = FontWeight.Bold,
                        color = colorResource(id = R.color.white),
                        textAlign = TextAlign.Center,
                        fontSize = 22.sp
                    )
                }
                Space()
                Row(modifier = modifierRow) {
                    Column(modifier = Modifier.weight(5f)) {
                        Row(modifier = modifierRow) {
                            Column(modifier = Modifier.weight(1f)) {
                                OutlinedButton(modifier = Modifier
                                    .fillMaxWidth()
                                    .height(56.dp),
                                    border = BorderStroke(1.dp, colorResource(id = R.color.gray)),
                                    shape = RoundedCornerShape(3.dp),
                                    onClick = { showDialog.value = true }
                                ) {
                                    Text(text = countryCode)
                                    Spacer(modifier = Modifier.padding(6.dp))
                                    Icon(imageVector = Icons.Filled.ArrowDropDown, contentDescription = "")
                                }
                            }
                            Spacer(modifier = Modifier.padding(4.dp))
                            Column(modifier = Modifier.weight(2f)) {
                                OutlinedTextField(
                                    modifier = modifierRow,
                                    value = phoneNumber,
                                    placeholder = { Text(text = "") },
                                    onValueChange = {
                                        phoneNumber = it
                                        coroutineScope.launch {
                                            loginViewModel.nextButton(2, phoneNumber)
                                        }
                                    },
                                    colors = OutlinedTextFieldDefaults.colors(
                                        focusedTextColor = colorResource(id = R.color.white),
                                        unfocusedTextColor = colorResource(id = R.color.white),
                                        unfocusedBorderColor = colorResource(id = R.color.gray),
                                        unfocusedLabelColor = Color.White,
                                        unfocusedLeadingIconColor = Color.White
                                    )
                                )
                            }
                        }
                    }
                    Column(modifier = Modifier
                        .weight(1f)
                        .align(Alignment.CenterVertically)) {
                        Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                            SetIconCheck(colorSelections)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun NextButton(buttonEnabled: Boolean, nextButton: () -> Unit) {
    Button(
        onClick = { nextButton() },
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp),
        shape = RoundedCornerShape(3.dp),
        colors = ButtonDefaults.buttonColors(
            containerColor = colorResource(id = R.color.colorGreenLight),
            disabledContainerColor = colorResource(id = R.color.gray),
            contentColor = Color.White,
            disabledContentColor = Color.White
        ),
        enabled = buttonEnabled
    ) {
        Text(text = stringResource(id = R.string.str_button_next))
    }
}

@Composable
fun Space() {
    Spacer(modifier = Modifier.padding(vertical = 16.dp))
}