package com.personal.fitness.ui.home.view.ui

import android.annotation.SuppressLint
import android.view.View
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.Divider
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults.topAppBarColors
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.commit
import com.personal.fitness.CoreApp.Companion.prefs
import com.personal.fitness.R
import com.personal.fitness.ui.home.view.fragments.CompleteDataFragment
import com.personal.fitness.ui.home.view.fragments.WorkoutFragment
import com.personal.fitness.utils.Prefs
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun HomeScreen(supportFragmentManager: FragmentManager) {
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()
    var completeData by rememberSaveable { mutableStateOf(prefs.getStateCompleteData()) }
    var viewShowing by rememberSaveable { mutableStateOf(prefs.getSharedName() ?: "Home") }
    var stateFitness by rememberSaveable { mutableStateOf(false) }
    var stateWorkout by rememberSaveable { mutableStateOf(true) }
    var stateRoutine by rememberSaveable { mutableStateOf(false) }
    CompositionLocalProvider(value = LocalLayoutDirection provides LayoutDirection.Rtl) {
        ModalNavigationDrawer(
            drawerState = drawerState,
            drawerContent = {
                ModalDrawerSheet {
                    Text(stringResource(id = R.string.str_fragment_title_menu), modifier = Modifier.padding(16.dp))
                    Divider()
                    NavigationDrawerItem(label = { Text(text = stringResource(id = R.string.str_fragment_title_favorite)) }, selected = stateFitness, onClick = {
                        if (!stateFitness) {
                            viewShowing = String.format("%s",R.string.str_fragment_title_favorite)
                            stateFitness = true
                            stateWorkout = false
                            stateRoutine = false
                        }
                        scope.launch {
                            drawerState.apply {
                                if (isClosed) open() else close()
                            }
                        }
                    })
                    NavigationDrawerItem(label = { Text(text = stringResource(id = R.string.str_fragment_title_workout)) }, selected = stateWorkout, onClick = {
                        if (!stateWorkout) {
                            viewShowing = String.format("%s",R.string.str_fragment_title_workout)
                            stateFitness = false
                            stateWorkout = true
                            stateRoutine = false
                        }
                        scope.launch {
                            drawerState.apply {
                                if (isClosed) open() else close()
                            }
                        }
                    })
                    NavigationDrawerItem(label = { Text(text = stringResource(id = R.string.str_fragment_title_routines)) }, selected = stateRoutine, onClick = {
                        if (!stateRoutine) {
                            viewShowing = String.format("%s",R.string.str_fragment_title_routines)
                            viewShowing = ""
                            stateFitness = false
                            stateWorkout = false
                            stateRoutine = true
                        }
                        scope.launch {
                            drawerState.apply {
                                if (isClosed) open() else close()
                            }
                        }
                    })
                }
            },
            gesturesEnabled = true
        ) {
            CompositionLocalProvider(value = LocalLayoutDirection provides LayoutDirection.Ltr) {
                Scaffold(topBar = { TopBar(scope, drawerState, viewShowing) }) { values ->
                    Surface(modifier = Modifier
                        .fillMaxSize()
                        .padding(values)) {
                        if (!completeData) {
                            viewShowing = stringResource(id = R.string.str_fragment_title_complete_data)
                            FragmentContainer(
                                modifier = Modifier.fillMaxSize(),
                                fragmentManager = supportFragmentManager,
                                commit = { add(it, CompleteDataFragment()) }
                            )
                        } else {
                            FragmentContainer(
                                modifier = Modifier.fillMaxSize(),
                                fragmentManager = supportFragmentManager,
                                commit = { add(it, WorkoutFragment()) }
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun FragmentContainer(
    modifier: Modifier = Modifier,
    fragmentManager: FragmentManager,
    commit: FragmentTransaction.(containerId: Int) -> Unit
) {
    val containerId by rememberSaveable { mutableIntStateOf(View.generateViewId()) }
    var initialized by rememberSaveable { mutableStateOf(false) }
    AndroidView(
        modifier = modifier,
        factory = { context -> FragmentContainerView(context).apply { id = containerId } },
        update = { view ->
            if (!initialized) {
                fragmentManager.commit { commit(view.id) }
                initialized = true
            } else {
                fragmentManager.onContainerAvailable(view)
            }
        }
    )
}

private fun FragmentManager.onContainerAvailable(view: FragmentContainerView) {
    val method = FragmentManager::class.java.getDeclaredMethod(
        "onContainerAvailable",
        FragmentContainerView::class.java
    )
    method.isAccessible = true
    method.invoke(this, view)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar(scope: CoroutineScope, drawerState: DrawerState, viewShowing: String) {
    TopAppBar(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                brush = Brush.horizontalGradient(
                    colors = listOf(
                        colorResource(id = R.color.black),
                        colorResource(id = R.color.black_background)
                    )
                )
            ),
        title = { Text(text = viewShowing) },
        colors = topAppBarColors(
            containerColor = Color.Transparent,
            titleContentColor = colorResource(id = R.color.white),
            navigationIconContentColor = colorResource(id = R.color.white),
            actionIconContentColor = colorResource(id = R.color.colorBlueDark)
        ),
        navigationIcon = {
            IconButton(
                onClick = {
                    scope.launch {
                        drawerState.apply {
                            if (isClosed) open() else close()
                        }
                    }
                }) {
                Icon(imageVector = Icons.Default.Menu, contentDescription = "Home section")
            }
        }
    )
}
