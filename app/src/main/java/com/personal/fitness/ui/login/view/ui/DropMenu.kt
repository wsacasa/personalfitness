package com.personal.fitness.ui.login.view.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.personal.fitness.R
import com.personal.fitness.ui.home.view.viewmodel.HomeViewModel

@Composable
fun DropMenu(viewModel: HomeViewModel, nGender: Array<String>) {
    var expanded by remember { mutableStateOf(false) }
    var comboGender by rememberSaveable { mutableStateOf("Female") }
    var comboSelect by rememberSaveable { mutableStateOf("") }
    Card(colors = CardDefaults.cardColors(
            containerColor = colorResource(id = R.color.black_down),
            contentColor = colorResource(id = R.color.gray),
            disabledContentColor = colorResource(id = R.color.gray),
            disabledContainerColor = colorResource(id = R.color.colorBlueDark)
        ),
        border = BorderStroke(1.dp, colorResource(id = R.color.gray)),
        shape = RoundedCornerShape(5.dp)
    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .height(55.dp)) {
            Column(modifier = Modifier.weight(2f)) {
                Box(modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally)) {
                    comboSelect = comboGender
                    Text(text = comboSelect, textAlign = TextAlign.Center, color = colorResource(id = R.color.gray_light), fontSize = 14.sp, modifier = Modifier
                        .fillMaxSize()
                        .wrapContentHeight(align = Alignment.CenterVertically))
                }
            }
            Column(modifier = Modifier.weight(1f)) {
                Box(modifier = Modifier.fillMaxSize()) {
                    IconButton(modifier = Modifier.fillMaxSize(), onClick = { expanded = !expanded }) {
                        Icon(imageVector = Icons.Default.ArrowDropDown, contentDescription = "Gender", tint = colorResource(id = R.color.gray_light))
                    }
                    DropdownMenu(
                        expanded = expanded,
                        onDismissRequest = { expanded = false }) {
                        for (rps in nGender) {
                            DropdownMenuItem(text = { Text(text = rps) }, onClick = {
                                expanded = false
                                comboGender = rps
                                viewModel.onGenderChanged(rps)
                            })
                        }
                    }
                }
            }
        }
    }
}