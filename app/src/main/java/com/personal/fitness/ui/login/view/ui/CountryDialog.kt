package com.personal.fitness.ui.login.view.ui

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.constraintlayout.compose.ConstraintLayout
import com.personal.fitness.R
import com.personal.fitness.utils.IconFlags
import com.personal.fitness.ui.home.view.viewmodel.HomeViewModel
import com.personal.fitness.ui.login.viewmodel.LoginViewModel

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CountryDialog(context: Context, viewModel: HomeViewModel?, loginModel: LoginViewModel?, setShowDialog: (Boolean) -> Unit) {
    Dialog(onDismissRequest = { setShowDialog(false) }) {
        Surface(modifier = Modifier
            .fillMaxWidth()
            .height(400.dp)) {
            ConstraintLayout(modifier = Modifier.fillMaxSize()) {
                val (header, content)  = createRefs()
                var find by rememberSaveable { mutableStateOf("") }
                OutlinedTextField(
                    value = find,
                    onValueChange = {
                        find = it
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(5.dp)
                        .constrainAs(header) {
                            top.linkTo(parent.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        },
                    enabled = true,
                    textStyle = TextStyle.Default.copy(textAlign = TextAlign.Start),
                    placeholder = { Text(text = "") },
                    leadingIcon = { Icon(imageVector = Icons.Default.Search, contentDescription = "") },
                    singleLine = true,
                    maxLines = 1,
                    colors = OutlinedTextFieldDefaults.colors(
                        focusedTextColor = colorResource(id = R.color.black_down),
                        unfocusedTextColor = colorResource(id = R.color.black_down),
                        unfocusedBorderColor = colorResource(id = R.color.gray),
                        unfocusedLabelColor = colorResource(id = R.color.black_down),
                        unfocusedLeadingIconColor = colorResource(id = R.color.black_down)
                    ),
                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Text),
                )
                LazyColumn(modifier = Modifier
                    .padding(horizontal = 5.dp, vertical = 20.dp)
                    .constrainAs(content) {
                        top.linkTo(header.bottom)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }) {
                    item {
                        //Spacer(modifier = Modifier.padding(5.dp))
                        /** *********************************************************************** */
                        val countries = mutableListOf(
                            "Afghanistan",
                            "Albania",
                            "Algeria",
                            "Andorra",
                            "Angola",
                            "Argentina",
                            "Armenia",
                            "Aruba",
                            "Australia",
                            "Austria",
                            "Azerbaijan",
                            "Bahrain",
                            "Bangladesh",
                            "Belarus",
                            "Belgium",
                            "Belize",
                            "Benin",
                            "Bhutan",
                            "Bolivia",
                            "Bosnia and Herzegovina",
                            "Botswana",
                            "Brazil",
                            "Brunei",
                            "Bulgaria",
                            "Cambodia",
                            "Cameroon",
                            "Canada",
                            "Chile",
                            "China",
                            "Colombia",
                            "Costa Rica",
                            "Croatia",
                            "Cuba",
                            "Czech Republic",
                            "Democratic Rep of the Congo",
                            "Denmark",
                            "Djibouti",
                            "Ecuador",
                            "Egypt",
                            "El Salvador",
                            "Estonia",
                            "Ethiopia",
                            "Fiji",
                            "Finland",
                            "France",
                            "Gabon",
                            "Gambia",
                            "Georgia",
                            "Germany",
                            "Ghana",
                            "Gibraltar",
                            "Greece",
                            "Greenland",
                            "Guatemala",
                            "Guinea",
                            "Guinea-Bissau",
                            "Guyana",
                            "Haiti",
                            "Honduras",
                            "Hong Kong",
                            "Hungary",
                            "Iceland",
                            "India",
                            "Indonesia",
                            "Iran",
                            "Iraq",
                            "Ireland",
                            "Israel",
                            "Italy",
                            "Jamaica",
                            "Japan",
                            "Jordan",
                            "Kazakhstan",
                            "Kenya",
                            "Kiribati",
                            "Kosovo",
                            "Kuwait",
                            "Laos",
                            "Latvia",
                            "Lebanon",
                            "Lesotho",
                            "Liberia",
                            "Libya",
                            "Liechtenstein",
                            "Lithuania",
                            "Luxembourg",
                            "Macau",
                            "Macedonia",
                            "Madagascar",
                            "Malawi",
                            "Malaysia",
                            "Maldives",
                            "Mali",
                            "Malta",
                            "Mauritania",
                            "Mexico",
                            "Micronesia",
                            "Moldova",
                            "Monaco",
                            "Mongolia",
                            "Montenegro",
                            "Morocco",
                            "Mozambique",
                            "Myanmar",
                            "Namibia",
                            "Nauru",
                            "Nepal",
                            "Netherlands",
                            "New Caledonia",
                            "New Zealand",
                            "Nicaragua",
                            "Niger",
                            "Nigeria",
                            "Niue",
                            "North Korea",
                            "Norway",
                            "Oman",
                            "Pakistan",
                            "Palau",
                            "Palestine",
                            "Panama",
                            "Paraguay",
                            "Peru",
                            "Philippines",
                            "Poland",
                            "Portugal",
                            "Puerto Rico",
                            "Puerto Rico",
                            "Qatar",
                            "Romania",
                            "Russia",
                            "Rwanda",
                            "Samoa",
                            "San Marino",
                            "Saudi Arabia",
                            "Senegal",
                            "Serbia",
                            "Singapore",
                            "Slovakia",
                            "Slovenia",
                            "Somalia",
                            "South Africa",
                            "South Korea",
                            "South Sudan",
                            "Spain",
                            "Sri Lanka",
                            "Sudan",
                            "Suriname",
                            "Swaziland",
                            "Sweden",
                            "Switzerland",
                            "Syria",
                            "Taiwan",
                            "Tajikistan",
                            "Tanzania",
                            "Thailand",
                            "Togo",
                            "Trinidad and Tobago",
                            "Turkey",
                            "Turkmenistan",
                            "Uganda",
                            "Ukraine",
                            "United Arab Emirates",
                            "United Kingdom",
                            "United States",
                            "Uruguay",
                            "Uzbekistan",
                            "Vatican",
                            "Venezuela",
                            "Vietnam",
                            "Yemen",
                            "Zambia",
                            "Zimbabwe")
                        val codes = mutableListOf(
                            "93",
                            "355",
                            "213",
                            "376",
                            "244",
                            "54",
                            "374",
                            "297",
                            "61",
                            "43",
                            "994",
                            "973",
                            "880",
                            "375",
                            "32",
                            "501",
                            "229",
                            "975",
                            "591",
                            "387",
                            "267",
                            "55",
                            "673",
                            "359",
                            "855",
                            "237",
                            "1",
                            "56",
                            "86",
                            "57",
                            "506",
                            "385",
                            "53",
                            "420",
                            "243",
                            "45",
                            "253",
                            "593",
                            "20",
                            "503",
                            "372",
                            "251",
                            "679",
                            "358",
                            "33",
                            "241",
                            "220",
                            "995",
                            "49",
                            "233",
                            "350",
                            "30",
                            "299",
                            "502",
                            "224",
                            "245",
                            "592",
                            "509",
                            "504",
                            "852",
                            "36",
                            "354",
                            "91",
                            "62",
                            "98",
                            "964",
                            "353",
                            "972",
                            "39",
                            "1876",
                            "81",
                            "962",
                            "7",
                            "254",
                            "686",
                            "383",
                            "965",
                            "856",
                            "371",
                            "961",
                            "266",
                            "231",
                            "218",
                            "423",
                            "370",
                            "352",
                            "853",
                            "389",
                            "261",
                            "265",
                            "60",
                            "960",
                            "223",
                            "356",
                            "222",
                            "52",
                            "691",
                            "373",
                            "377",
                            "976",
                            "382",
                            "212",
                            "258",
                            "95",
                            "264",
                            "674",
                            "977",
                            "31",
                            "687",
                            "64",
                            "505",
                            "227",
                            "234",
                            "683",
                            "850",
                            "47",
                            "968",
                            "92",
                            "680",
                            "970",
                            "507",
                            "595",
                            "51",
                            "63",
                            "48",
                            "351",
                            "1787",
                            "1939",
                            "974",
                            "40",
                            "7",
                            "250",
                            "685",
                            "378",
                            "966",
                            "221",
                            "381",
                            "65",
                            "421",
                            "386",
                            "252",
                            "27",
                            "82",
                            "211",
                            "34",
                            "94",
                            "249",
                            "597",
                            "268",
                            "46",
                            "41",
                            "963",
                            "886",
                            "992",
                            "255",
                            "66",
                            "228",
                            "1868",
                            "90",
                            "993",
                            "256",
                            "380",
                            "971",
                            "44",
                            "1",
                            "598",
                            "998",
                            "379",
                            "58",
                            "84",
                            "967",
                            "260",
                            "263")
                        var listCodes = mutableListOf<String>()
                        var listCountries = mutableListOf<String>()
                        var listFlags = ArrayList<Bitmap>()
                        val iconFlags = IconFlags(context)
                        val iIcons = iconFlags.getListFlags()
                        val numberFlag = iIcons.size
                        if (find != "") {
                            listFlags.clear()
                            for (item in countries.indices){
                                if (countries[item].contains(find, ignoreCase = true)) {
                                    listFlags.add(iIcons[item])
                                    listCountries.add(countries[item])
                                    listCodes.add(codes[item])
                                }
                            }
                        } else {
                            listFlags = iconFlags.getListFlags()
                            listCodes = codes
                            listCountries = countries
                        }
                        var imageIcon = listFlags[0].asImageBitmap()
                        for (item in listCountries.indices) {
                            Row(modifier = Modifier.fillMaxWidth()) {
                                Column(modifier = Modifier.align(Alignment.Top)) {
                                    TextButton(
                                        modifier = Modifier.fillMaxWidth(),
                                        shape = RoundedCornerShape(3.dp),
                                        colors = ButtonDefaults.buttonColors(
                                            containerColor = Color.Transparent,
                                            contentColor = colorResource(id = R.color.black_down),
                                            disabledContainerColor = Color.Transparent,
                                            disabledContentColor = Color.Transparent
                                        ),
                                        onClick = {
                                            if (viewModel != null) {
                                                viewModel.onCodePhone(String.format("+ ${listCodes[item]}"))
                                            } else {
                                                loginModel.onCodePhone(String.format("+ ${listCodes[item]}"))
                                            }
                                            setShowDialog(false)
                                        }) {
                                        Row(modifier = Modifier.fillMaxWidth().align(Alignment.CenterVertically)) {
                                            Log.d("CountryDialog", "CountryDialog: $item")
                                            if (item < numberFlag) {
                                                imageIcon = listFlags[item].asImageBitmap()
                                            }
                                            Image(modifier = Modifier
                                                .width(30.dp)
                                                .height(30.dp), bitmap = imageIcon, contentDescription = "", alignment = Alignment.Center)
                                            Spacer(modifier = Modifier.padding(4.dp))
                                            Text(text = listCodes[item], modifier = Modifier.width(40.dp), textAlign = TextAlign.Start)
                                            Spacer(modifier = Modifier.padding(4.dp))
                                            Text(text = listCountries[item], textAlign = TextAlign.Start)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
