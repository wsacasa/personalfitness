package com.personal.fitness.ui.home.view.fragments

import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.stringResource
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.personal.fitness.CoreApp
import com.personal.fitness.R
import com.personal.fitness.ui.home.view.viewmodel.HomeViewModel
import com.personal.fitness.ui.login.view.ui.CompleteDataScreen
import com.personal.fitness.ui.theme.PersonalFitnessTheme

class CompleteDataFragment: Fragment() {
    private val homeViewModel: HomeViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return ComposeView(requireContext()).apply { 
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent { 
                PersonalFitnessTheme {
                    val name = CoreApp.prefs.getSharedName() ?: stringResource(id = R.string.str_next_data_name)
                    //homeViewModel.getPhoneMobile()
                    CompleteDataScreen(name = name, viewModel = homeViewModel, context = requireContext())
                }
            }
        }
    }
}