package com.personal.fitness.ui.login.view.ui

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Slider
import androidx.compose.material3.SliderDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.personal.fitness.CoreApp.Companion.prefs
import com.personal.fitness.R
import com.personal.fitness.ui.home.view.viewmodel.HomeViewModel
import kotlin.math.pow
import kotlin.math.roundToInt

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CompleteDataScreen(name: String, viewModel: HomeViewModel, context: Context) {
    Surface(modifier = Modifier.fillMaxSize()) {
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            item {
                MainContent(name, viewModel, context)
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun MainContent(name: String, viewModel: HomeViewModel, context: Context) {
    ConstraintLayout (modifier = Modifier
        .fillMaxSize()
        .background(color = colorResource(id = R.color.black_background))) {
        val (header, content)  = createRefs()
        Box(modifier = Modifier
            .constrainAs(header) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        ) {
            Title()
        }
        Box(modifier = Modifier
            .padding(10.dp)
            .constrainAs(content) {
                top.linkTo(header.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        ) {
            Column {
                CompleteName(name, viewModel, context) //names
            }
        }
    }
}

@Composable
fun DataPicker(viewModel: HomeViewModel) {
    Surface(modifier = Modifier.fillMaxWidth()) {
        Column(horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center, modifier = Modifier
            .fillMaxWidth()
            .height(130.dp)
            .background(colorResource(id = R.color.black_down))
        ) {
            val values = remember { (12..90).map { it.toString() } }
            val valuesPickerState = rememberPickerState()
            val units = remember { (150..250).map { it.toString() } }
            val unitsPickerState = rememberPickerState()
            Row(modifier = Modifier.fillMaxWidth()) {
                Column(modifier = Modifier.weight(1f)) {
                    Text(text = stringResource(id = R.string.str_next_data_age_title), color = colorResource(id = R.color.gray_light), modifier = Modifier
                        .padding(top = 4.dp)
                        .align(Alignment.CenterHorizontally))
                    Picker(modifier = Modifier.weight(0.3f), items = values, state = valuesPickerState, visibleItemsCount = 3, textModifier = Modifier.padding(8.dp), textStyle = TextStyle(fontSize = 14.sp), viewModel = viewModel, typeSelected = 0)
                }
                Spacer(modifier = Modifier.padding(10.dp))
                Column(modifier = Modifier.weight(1f)) {
                    Text(text = stringResource(id = R.string.str_next_data_height_title), color = colorResource(id = R.color.gray_light), modifier = Modifier
                        .padding(top = 4.dp)
                        .align(Alignment.CenterHorizontally))
                    Picker(modifier = Modifier.weight(0.3f), items = units, state = unitsPickerState, visibleItemsCount = 3, textModifier = Modifier.padding(8.dp), textStyle = TextStyle(fontSize = 14.sp), viewModel = viewModel, typeSelected = 1)
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun CompleteName(name: String, viewModel: HomeViewModel, context: Context) {
    val cardColors = CardDefaults.cardColors(
        containerColor = colorResource(id = R.color.black_down),
        contentColor = colorResource(id = R.color.gray),
        disabledContentColor = colorResource(id = R.color.gray),
        disabledContainerColor = colorResource(id = R.color.colorBlueDark))
    val modifierBox = Modifier
        .fillMaxWidth()
        .padding(12.dp)
    val modifierRow = Modifier.fillMaxWidth()
    var phoneNumber = ""
    val showDialog = remember { mutableStateOf(false) }
    var nameChanged by rememberSaveable { mutableStateOf("") }
    var nameHolder by rememberSaveable { mutableStateOf(name) }
    var weightHolder by rememberSaveable { mutableStateOf("0") }
    var weightValue by rememberSaveable { mutableFloatStateOf(0f) }
    val countryCode : String by viewModel.code.observeAsState(initial = "+ 0")
    val age : Int by viewModel.age.observeAsState(initial = 12)
    val height : Int by viewModel.height.observeAsState(initial = 150)
    val gender : String by viewModel.gender.observeAsState(initial = "")

    val colorSelections = arrayOf(colorResource(id = R.color.gray), colorResource(id = R.color.gray), colorResource(id = R.color.gray), colorResource(id = R.color.gray))
    var color = Color.Transparent
    var infoText = ""
    if (name != stringResource(id = R.string.str_next_data_name)) {
        color = colorResource(id = R.color.colorBlueDark)
        infoText = stringResource(id = R.string.str_next_data_title_info)
    }
    if (age > 12) {
        if (nameChanged == "") {
            if (name != stringResource(id = R.string.str_next_data_name)) {
                nameChanged = name
                viewModel.onNamedChanged(nameChanged)
            }
        }
        colorSelections[0] = colorResource(id = R.color.colorGreenLight)
        if (height > 150) {
            colorSelections[2] = colorResource(id = R.color.colorGreenLight)
        }
    } else {
        colorSelections[0] = colorResource(id = R.color.colorGreenLight)
        if (height > 150) {
            colorSelections[2] = colorResource(id = R.color.colorGreenLight)
        }
    }
    if (gender != "") {
        if (weightValue <= 0f) {
            val result = if (gender ==  "Female"){ (((height.toFloat()/100).pow(2)) * 21.5) } else { (((height.toFloat()/100).pow(2)) * 23.0) }
            val rounded = (result * 100.0).roundToInt() / 100.0
            weightValue = rounded.toFloat()
            viewModel.onWeightSelected(weightValue)
        }
        colorSelections[3] = colorResource(id = R.color.colorGreenLight)
    }
    if (showDialog.value) {
        CountryDialog(context = context, viewModel = viewModel, loginModel = null, setShowDialog = { showDialog.value = it })
    }
    Detail(name, nameChanged, prefs.getUserEmail(context), age, height, weightValue, gender)
    SetSpacer()
    /** Full Name */
    Card(colors = cardColors) {
        Box(modifier = modifierBox) {
            Column {
                /** Icon Data */
                Row(modifier = modifierRow) {
                    Column {
                        Icon(modifier = Modifier
                            .height(15.dp)
                            .padding(end = 3.dp, bottom = 3.dp), imageVector = Icons.Default.Info, tint = color, contentDescription = stringResource(id = R.string.str_general_info))
                    }
                    Column {
                        Text(text = infoText, fontSize = 12.sp, color = colorResource(id = R.color.colorBlueDark))
                    }
                }
                Row(modifier = modifierRow) {
                    Column(modifier = Modifier.weight(5f)) {
                        OutlinedTextField(
                            modifier = modifierRow,
                            value = nameChanged,
                            placeholder = { Text(text = nameHolder) },
                            onValueChange = {
                                if (nameHolder != "") { nameHolder = "" }
                                nameChanged = it
                                viewModel.onNamedChanged(nameChanged)
                            },
                            colors = OutlinedTextFieldDefaults.colors(
                                focusedTextColor = colorResource(id = R.color.white),
                                unfocusedTextColor = colorResource(id = R.color.white),
                                unfocusedBorderColor = colorResource(id = R.color.gray),
                                unfocusedLabelColor = Color.White,
                                unfocusedLeadingIconColor = Color.White
                            )
                        )
                    }
                    Column(modifier = Modifier
                        .weight(1f)
                        .align(Alignment.CenterVertically)) {
                        Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                            SetIconCheck(colorSelections[0])
                        }
                    }
                }
            }

        }
    }
    SetSpacer()
    /** Phone Number */
    Card(colors = cardColors){
        Box(modifier = modifierBox) {
            Row(modifier = modifierRow) {
                Column(modifier = Modifier.weight(5f)) {
                    Row(modifier = modifierRow) {
                        Column(modifier = Modifier.weight(1f)) {
                            OutlinedButton(modifier = Modifier
                                .fillMaxWidth()
                                .height(55.dp),
                                border = BorderStroke(1.dp, colorResource(id = R.color.gray)),
                                shape = RoundedCornerShape(3.dp),
                                onClick = { showDialog.value = true }
                            ) {
                                Text(text = countryCode)
                                Spacer(modifier = Modifier.padding(6.dp))
                                Icon(imageVector = Icons.Filled.ArrowDropDown, contentDescription = "")
                            }
                        }
                        Spacer(modifier = Modifier.padding(4.dp))
                        Column(modifier = Modifier.weight(2f)) {
                            OutlinedTextField(
                                modifier = modifierRow,
                                value = phoneNumber,
                                placeholder = { Text(text = "") },
                                onValueChange = {
                                    /**if (nameHolder != "") { nameHolder = "" }*/
                                    phoneNumber = it
                                    viewModel.onPhoneNumber(phoneNumber)
                                },
                                colors = OutlinedTextFieldDefaults.colors(
                                focusedTextColor = colorResource(id = R.color.white),
                                unfocusedTextColor = colorResource(id = R.color.white),
                                unfocusedBorderColor = colorResource(id = R.color.gray),
                                unfocusedLabelColor = Color.White,
                                unfocusedLeadingIconColor = Color.White
                                )
                            )
                        }
                    }
                }
                Column(modifier = Modifier
                    .weight(1f)
                    .align(Alignment.CenterVertically)) {
                    Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                        SetIconCheck(colorSelections[1])
                    }
                }
            }
        }
    }
    SetSpacer()
    /** Age Section */
    Card(colors = cardColors) {
        Box(modifier = modifierBox) {
            Row(modifier = modifierRow) {
                Column(modifier = Modifier.weight(5f)) {
                    DataPicker(viewModel)
                }
                Column(modifier = Modifier
                    .weight(1f)
                    .align(Alignment.CenterVertically)) {
                    Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                        SetIconCheck(colorSelections[2])
                    }
                }
            }
        }
    }
    SetSpacer()
    /** Weight Section */
    Card(colors = cardColors) {
        Box(modifier = modifierBox) {
            Row(modifier = modifierRow) {
                Column(modifier = Modifier.weight(5f)) {
                    Row(modifier = Modifier.fillMaxWidth()) {
                        Column(modifier = Modifier.weight(1f)) {
                            Text(text = stringResource(id = R.string.str_next_data_weight_title), color = colorResource(id = R.color.gray_light), modifier = Modifier
                                .padding(bottom = 10.dp)
                                .align(Alignment.CenterHorizontally))
                            Row {
                                OutlinedTextField(modifier = modifierRow,
                                    value = weightValue.toString(),
                                    placeholder = { Text(text = weightHolder, fontSize = 12.sp) },
                                    singleLine = true,
                                    maxLines = 1,
                                    colors = OutlinedTextFieldDefaults.colors(
                                        focusedTextColor = colorResource(id = R.color.white),
                                        unfocusedTextColor = colorResource(id = R.color.white),
                                        unfocusedBorderColor = colorResource(id = R.color.gray),
                                        unfocusedLabelColor = Color.White,
                                        unfocusedLeadingIconColor = Color.White
                                    ),
                                    textStyle = TextStyle.Default.copy(textAlign = TextAlign.Center),
                                    keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Decimal),
                                    onValueChange = {
                                        if (weightHolder != "0") weightHolder = ""
                                        weightValue = it.toFloat()
                                        viewModel.onWeightSelected(weightValue)
                                    })
                            }
                            Row {
                                Slider(
                                    modifier = modifierRow,
                                    value = weightValue,
                                    onValueChange = {
                                        weightValue = it
                                    },
                                    colors = SliderDefaults.colors(
                                        thumbColor = colorResource(id = R.color.white),
                                        activeTrackColor = colorResource(id = R.color.gray),
                                        inactiveTrackColor = colorResource(id = R.color.black_background)
                                    )
                                )
                            }
                        }
                        Spacer(modifier = Modifier.padding(4.dp))
                        Column(modifier = Modifier.weight(2f)) {
                            Text(text = "Gender", color = colorResource(id = R.color.gray_light), modifier = Modifier
                                .padding(bottom = 10.dp)
                                .align(Alignment.CenterHorizontally))
                            val nGender = arrayOf("Female", "Male")
                            DropMenu(viewModel, nGender)
                        }
                    }
                }
                Column(modifier = Modifier
                    .weight(1f)
                    .align(Alignment.CenterVertically)) {
                    Row(modifier = Modifier.align(Alignment.CenterHorizontally)) {
                        SetIconCheck(colorSelections[3])
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun OutLineButt() {
    OutlinedButton(modifier = Modifier.fillMaxWidth(),
        border = BorderStroke(1.dp, colorResource(id = R.color.gray)),
        shape = RoundedCornerShape(3.dp),
        onClick = {  }
    ) {
        Text(text = "+ 0")
        Spacer(modifier = Modifier.padding(6.dp))
        Icon(imageVector = Icons.Filled.ArrowDropDown, contentDescription = "")
    }
}

@Composable
fun SetIconCheck(color: Color) {
    Image(
        imageVector = Icons.Default.CheckCircle,
        contentDescription = stringResource(id = R.string.str_next_data_name),
        alignment = Alignment.Center,
        colorFilter = ColorFilter.tint(color = color)
    )
}

@Composable
private fun SetSpacer() {
    Spacer(modifier = Modifier.padding(6.dp))
}

@Composable
fun Title() {
    Text(modifier = Modifier.fillMaxWidth(),
        text = stringResource(id = R.string.str_next_data_title),
        textAlign = TextAlign.Justify,
        fontSize = 10.sp,
        color = colorResource(id = R.color.white))
}

@Composable
fun Detail(name: String, nameChanged: String, email: String, age: Int, height: Int, weightValue: Float, gender: String) {
    Card(colors = CardDefaults.cardColors(
        containerColor = colorResource(id = R.color.black_down),
        contentColor = colorResource(id = R.color.white),
        disabledContentColor = colorResource(id = R.color.gray),
        disabledContainerColor = colorResource(id = R.color.colorBlueDark)
    )) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp)) {
            Column(modifier = Modifier
                .weight(1f)
                .align(Alignment.CenterVertically)) {
                Image(painter = painterResource(id = R.drawable.fitness_icon), contentDescription = "User Image", modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp))
            }
            Column(modifier = Modifier
                .weight(2f)
                .align(Alignment.CenterVertically)) {
                if (nameChanged != "") Text(text = nameChanged) else Text(text = name)
                Spacer(modifier = Modifier.padding(2.dp))
                Text(text = email)
                Spacer(modifier = Modifier.padding(2.dp))
                Text(text = "")
                Divider(color = colorResource(id = R.color.gray))
                Spacer(modifier = Modifier.padding(2.dp))
                Row {
                    Column(modifier = Modifier.weight(1f)) {
                        Row {
                            Text(text = String.format("%s: ", stringResource(id = R.string.str_next_data_age_title)))
                            Text(text = String.format("%s", age), fontWeight = FontWeight.Bold)
                        }
                        Spacer(modifier = Modifier.padding(2.dp))
                        Row {
                            Text(text = String.format("%s: ", stringResource(id = R.string.str_next_data_weight_title)))
                            Text(text = String.format("%s", weightValue), fontWeight = FontWeight.Bold)
                        }
                    }
                    Column(modifier = Modifier.weight(1f)) {
                        Row {
                            Text(text = String.format("%s: ", stringResource(id = R.string.str_next_data_height_title_)))
                            Text(text = String.format("%s", height), fontWeight = FontWeight.Bold)
                        }
                        Spacer(modifier = Modifier.padding(2.dp))
                        Row {
                            Text(text = String.format("%s: ", stringResource(id = R.string.str_next_data_gender_title)))
                            Text(text = String.format("%s", gender), fontWeight = FontWeight.Bold)
                        }
                    }
                }
            }
        }
    }
}
