package com.personal.fitness.ui.login.view.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.personal.fitness.CoreApp
import com.personal.fitness.CoreApp.Companion.prefs
import com.personal.fitness.R
import com.personal.fitness.data.login.LoginData
import com.personal.fitness.ui.home.view.activities.MainActivity
import com.personal.fitness.ui.login.view.ui.LoginScreen
import com.personal.fitness.ui.login.viewmodel.LoginViewModel
import com.personal.fitness.ui.theme.PersonalFitnessTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity: ComponentActivity() {
    /** ViewModel Variables */
    private val loginViewModel: LoginViewModel by viewModels()

    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private var resultSign: ActivityResultLauncher<Intent>? = null

    private var logout: Boolean = false
    private var logoutType: String = "none"
    private var user: String = ""
    private var pass: String = ""
    private var type: String = ""
    private var urlI: String = "none"
    private var name: String = "none"

    companion object {
        private const val TAG: String = "LoginActivity"
        var LOGOUT: String = "Logouts"
        var LOGOUTYPE: String = "LogoutType"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Configure sign-in to request the user's ID, email address, and basic profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        logout = intent.getBooleanExtra(LOGOUT, false)
        logoutType = intent.getStringExtra(LOGOUTYPE).toString()

        //Google Sign In Result
        resultSign = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            }
            Log.d(TAG, "onCreate: $result - ${result.data}")
        }

        setContent {
            PersonalFitnessTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.onSurface
                ) {
                    LoginScreen(viewModel = loginViewModel)
                }
            }
        }

        loginViewModel.googleSingI.observe(this) {
            if (it) {
                val signInIntent: Intent = mGoogleSignInClient.signInIntent
                resultSign?.launch(signInIntent)
            }
        }

        loginViewModel.firebaseLogin.observe(this) { data ->
            loginViewModel.submitData(data)
        }

        loginViewModel.userSuccessfullySave.observe(this) { data ->
            data?.let { response ->
                response.data?.let {
                    initSession(response.state, response.data)
                    //nextData(response.state, response.data)
                }
            }
        }
    }

    /*private fun nextData(state: Int, data: LoginData) {
        loginViewModel.nextData(state, data)
    }*/

    private fun initSession(state: Int, data: LoginData) {
        if (state == 0) {
            /** Save in cache the document associate to user */
            prefs.saveUserIdentity(data.doc)
            if (data.name != ""){
                prefs.saveUserName(data.name)
                prefs.saveUserEmail(data.user)
            }
        }
        /** Redirect to MainActivity (Home Screen) */
        val intent = Intent(this@LoginActivity, MainActivity::class.java)
        intent.putExtra("user", data.user)
        intent.putExtra("pass", data.pass)
        intent.putExtra("type", data.type)
        intent.putExtra("name", data.name)
        intent.putExtra("url", data.url)
        intent.putExtra("doc", data.doc)
        loginViewModel.onLoadingDismiss()
        startActivity(intent)
        finish()
    }

    @Deprecated("Deprecated in Java", ReplaceWith("super.onActivityResult(requestCode, resultCode, data)", "androidx.activity.ComponentActivity"))
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            // Signed in successfully, show authenticated UI.
            Log.d(TAG, String.format("updateUI: private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>)"))
            verifiedGoogleSession(0, account)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.d(TAG, "updateUI: signInResult = failed code=" + e.statusCode)
            verifiedGoogleSession(null, null)
            val title = String.format("Failed SignIn code = ${e.statusCode}")
            val content = e.message.toString()
            loginViewModel.showDialog(title, content, true, null)
        }
    }

    private fun verifiedGoogleSession(googleState: Int?, account: GoogleSignInAccount?) {
        var loginData: LoginData? = null
        if (account != null) {
            Log.d(TAG, String.format("updateUI: %s - %s, %s, %s, %s", account.familyName, account.givenName, account.displayName, account.email, account.photoUrl))
            name = account.displayName.toString()
            urlI = account.photoUrl.toString()
            user = account.email.toString()
            pass = "gAccount"
            type = "google"
            prefs.saveUserName(name)
            loginData = LoginData(user = user, pass = pass, type = type, name = name, url = urlI, doc = "")
            googleState?.let {
                initSession(googleState, loginData)
                //nextData(googleState, loginData)
            }
        } else {
            Log.d(TAG, String.format("updateUI: Account null"))
        }
        val title = getString(R.string.str_msg_title)
        val content = String.format("%s\n%s\n%s", loginData?.name.toString(), loginData?.user.toString(), loginData?.type.toString())
        loginViewModel.showDialog(title, content, true, loginData)
    }

    override fun onStart() {
        super.onStart()
        if (!logout) {
            // Check for existing Google Sign In account, if the user is already signed in the GoogleSignInAccount will be non-null.
            val account = GoogleSignIn.getLastSignedInAccount(this)
            Log.d(TAG, String.format("updateUI: override fun onStart()"))
            if (account != null) {
                verifiedGoogleSession(1, account)
            } else {
                val userIdentity = prefs.getUserIdentity()
                if (userIdentity != null) loginViewModel.initFirebaseData(userIdentity)
                /*accessToken = AccessToken.getCurrentAccessToken()
                val request: GraphRequest = GraphRequest.newMeRequest(accessToken
                ) { obj, _ ->
                    try {
                        url = obj?.getJSONObject("picture")?.getJSONObject("data")?.getString("url").toString()
                        name = obj?.getString("name").toString()
                        user = obj?.getString("link").toString()
                        pass = obj?.getString("id").toString()
                        type = "facebook"
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id,name,link,picture.type(large)")
                request.parameters = parameters
                request.executeAsync()

                if (accessToken != null && accessToken?.isExpired == false) {
                    userData = UserDatas(user, pass, type, name, url)
                    initSession(null)
                }*/
            }
        } else {
            //TODO Evaluar cuando entra con formulario para evitar breakpoints
            when (logoutType) {
                "facebook" -> {
                    //singOutFacebook()
                }

                "google" -> {
                    signOutGoogle()
                }
            }
        }
    }

    private fun signOutGoogle() {
        mGoogleSignInClient.signOut()
            .addOnCompleteListener(this) {
                revokeAccess()
            }
    }

    private fun revokeAccess() {
        mGoogleSignInClient.revokeAccess()
            .addOnCompleteListener(this) {
                val title = getString(R.string.str_msg_title)
                val content = getString(R.string.str_logout_msg)
                loginViewModel.showDialog(title, content, true, null)
                Toast.makeText(this, R.string.str_logout_msg, Toast.LENGTH_LONG).show()
            }
    }
}