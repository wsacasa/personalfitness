package com.personal.fitness.data.login

import com.google.gson.annotations.SerializedName

data class LoginData(
    @SerializedName("user") var user: String,
    @SerializedName("pass") val pass: String,
    @SerializedName("type") val type: String,
    @SerializedName("name") val name: String,
    @SerializedName("url") val url: String,
    @SerializedName("doc") var doc: String
)