package com.personal.fitness.data.login

data class LoginResponse(val state: Int, val data: LoginData?)