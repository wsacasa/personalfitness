package com.personal.fitness.data.login

data class CustomDialogData(val title: String, val content: String, var state: Boolean, val user: LoginData?)