package com.personal.fitness.data

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.personal.fitness.data.home.WorkoutData
import com.personal.fitness.data.login.LoginData
import com.personal.fitness.ui.home.view.viewmodel.HomeViewModel
import com.personal.fitness.ui.login.viewmodel.LoginViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FirestoreManager @Inject constructor(private val loginViewModel: LoginViewModel?, private val homeViewModel: HomeViewModel?) {
    private val firestore = FirebaseFirestore.getInstance()

    companion object {
        private const val TAG: String = "FirebaseManager"
    }

    suspend fun addUser(loginData: LoginData) {
        firestore.collection("tb_login").add(loginData).await().addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }
            val source = if (snapshot != null && snapshot.metadata.hasPendingWrites()) "Local" else "Server"
            if (snapshot != null && snapshot.exists()) {
                Log.d(TAG, "$source data: ${snapshot.data}")
                Log.d(TAG, "$source data: ${snapshot.id}")
                loginData.doc = snapshot.id
                CoroutineScope(Dispatchers.IO).launch {
                    withContext(Dispatchers.IO) {
                        if (loginViewModel != null) {
                            loginViewModel.userSaved(loginData)
                            updateUser(loginData)
                        }
                    }
                }
            } else {
                Log.d(TAG, "$source data: null")
            }
        }
    }

    suspend fun updateUser(loginData: LoginData) {
        val userRef = loginData.doc.let { firestore.collection("tb_login").document(it) }
        userRef.set(loginData).await()
    }

    suspend fun deleteUser(user: String) {
        val userRef = firestore.collection("tb_login").document(user)
        userRef.delete().await()
    }

    suspend fun getUserFlow(identity: String): Flow<List<LoginData>> = callbackFlow {
        val userRef = firestore.collection("tb_login").whereEqualTo("doc",identity).orderBy("user")
        /** Listener and Subscription */
        val subscription = userRef.addSnapshotListener { snapshot, _ ->
            snapshot?.let { querySnapshot ->  
                val users = mutableListOf<LoginData>()
                for (document in querySnapshot.documents) {
                    val userLogin = document.toObject(LoginData::class.java)
                    userLogin?.doc = document.id
                    userLogin?.let { users.add(it) }
                }
                trySend(users).isSuccess
            }
        }
        awaitClose { subscription.remove() }
    }

    suspend fun addWorkout(data: WorkoutData) {
        firestore.collection("tb_workout").add(data).await().addSnapshotListener { value, error ->
            if (error != null) {
                Log.d(TAG, "addWorkout: Listen failed", error)
                return@addSnapshotListener
            }
            //val source = if (value != null && value.metadata.hasPendingWrites()) "Local" else "Server"
            if (value != null && value.exists()) {
                CoroutineScope(Dispatchers.IO).launch {
                    withContext(Dispatchers.IO) {
                        if (value.id != "") {
                            val workoutId = value.id
                            data.id = workoutId
                            if (homeViewModel != null) {
                                homeViewModel.workoutSaved(workoutId)
                                updateWorkout(data)
                            }
                        }
                    }
                }
            } else {
                Log.d(TAG, "addWorkout: null")
            }
        }
    }

    suspend fun updateWorkout(data: WorkoutData) {
        val userRef = data.id.let { firestore.collection("tb_login").document(it) }
        userRef.set(data).await()
    }

    suspend fun deleteWorkout(data: String) {
        val userRef = firestore.collection("tb_login").document(data)
        userRef.delete().await()
    }
}