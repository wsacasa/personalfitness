package com.personal.fitness.data.home

import com.google.gson.annotations.SerializedName

data class WorkoutData(
    @SerializedName("id") var id: String,
    @SerializedName("name") val name: String,
    @SerializedName("body") val body: String,
    @SerializedName("series") val series: Int,
    @SerializedName("reps") val reps: Int,
    @SerializedName("instrument") val instrument: String,
    @SerializedName("description") val description: String,
    @SerializedName("image") val image: String,
    @SerializedName("user") val user: String,
    @SerializedName("date") val date: String
)