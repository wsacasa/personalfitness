package com.personal.fitness.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import java.io.IOException
import java.io.InputStream
import java.nio.file.Files

@RequiresApi(Build.VERSION_CODES.O)
class IconFlags(var context: Context) {
    init {
        val assetManager = context.assets
        try {
            var iIcons: InputStream
            var files = assetManager.list("flags")
            if (files != null) {
                for (i in files.indices) {
                    iIcons = assetManager.open("flags/" + files[i])
                    val bitmap = BitmapFactory.decodeStream(iIcons)
                    iconListFlag.add(i, bitmap)
                    iconListFlagName.add(i, files[i])
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
            Log.d(TAG, ": ${e.message}")
        }
    }

    fun getListFlags(): ArrayList<Bitmap> {
        return iconListFlag
    }

    companion object {
        private const val TAG: String = "Icons"
        var iconListFlag: ArrayList<Bitmap> = ArrayList()
        var iconListFlagName: ArrayList<String> = ArrayList()
    }
}