package com.personal.fitness.utils

import android.content.Context
import androidx.compose.ui.res.stringResource
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getString
import com.personal.fitness.R

class Prefs(context: Context) {

    private val SHARED_USER_URL = "shared_user_url"
    private val SHARED_DATA_PRODUCT = "shared_data_product"

    companion object {
        private const val SHARED_NAME = "Sdb"
        private const val SHARED_USER_IDENTITY = "shared_user_identity"
        private const val SHARED_USER_NAME = "shared_user_name"
        private const val SHARED_USER_EMAIL = "shared_user_email"
        private const val SHARED_COMPLETE_DATA = "shared_complete_data"
        private const val SHARED_USER_CODE = "type_user_code"
    }
    private val storage = context.getSharedPreferences(SHARED_NAME, 0)

    fun saveProductsData(data: String) {
        storage.edit().putString(SHARED_DATA_PRODUCT, data).apply()
    }

    fun saveUserUrl(url: String) {
        storage.edit().putString(SHARED_USER_URL, url).apply()
    }

    fun saveUserCode(type: String) {
        storage.edit().putString(SHARED_USER_CODE, type).apply()
    }

    fun getProductData(): String? {
        return storage.getString(SHARED_DATA_PRODUCT, "none")
    }

    fun getSharedUrl(): String? {
        return storage.getString(SHARED_USER_URL, "none")
    }

    fun getUserCode(): String {
        return storage.getString(SHARED_USER_CODE, "0")!!
    }

    /** Get user Firebase identity */
    fun getUserIdentity(): String? {
        return storage.getString(SHARED_USER_IDENTITY, null)
    }
    fun saveUserIdentity(user: String) {
        storage.edit().putString(SHARED_USER_IDENTITY, user).apply()
    }

    /** Get the name of user */
    fun getSharedName(): String? {
        return storage.getString(SHARED_USER_NAME, null)
    }

    fun saveUserName(name: String) {
        storage.edit().putString(SHARED_USER_NAME, name).apply()
    }

    /** Get User Email */
    fun getUserEmail(context: Context): String {
        return storage.getString(SHARED_USER_EMAIL, getString(context, R.string.str_not_detected)).toString()
    }

    fun saveUserEmail(email: String) {
        storage.edit().putString(SHARED_USER_EMAIL, email).apply()
    }

    /** Get State Complete Data */
    fun getStateCompleteData(): Boolean {
        return storage.getBoolean(SHARED_COMPLETE_DATA, false)
    }

    fun saveStateCompleteData(state: Boolean) {
        storage.edit().putBoolean(SHARED_COMPLETE_DATA, state).apply()
    }

}